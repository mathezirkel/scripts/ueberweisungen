# ueberweisungen

Script to match incoming bank transfers to required ones

## Getting started

You need

* Python 3.9
* Two input files:
  * Bank statement in MT490 format
  * List of required transfers

To execute the script and update the list of required transfers, execute

```shell
$ python src/main.py --bank-statement=bank-statement.csv --transfers=transfers.csv
```

## License

`ueberweisungen` is licensed under GPL-3, see [LICENSE](LICENSE).
