# ueberweisungen
# Copyright (C) 2022  Mathezirkel Augsburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from __future__ import annotations

from decimal import Decimal
from dataclasses import dataclass
from typing import Dict


@dataclass(frozen=True)
class Ueberweisung:
    person: str
    betrag: Decimal
    betreff: str

    @classmethod
    def from_dict(cls, dictionary: Dict[str, str]) -> Ueberweisung:
        return Ueberweisung(
            person=dictionary['Beguenstigter/Zahlungspflichtiger'],
            betrag=Decimal(dictionary['Betrag'].replace(',', '.')),
            betreff=dictionary['Verwendungszweck']
        )


@dataclass(frozen=True)
class Teilnehmer:
    nachname: str
    vorname: str
    preis: Decimal
    bereits_bezahlt: Decimal
    email_eltern: str
    email_schueler: str

    @classmethod
    def from_dict(cls, dictionary: Dict[str, str]) -> Teilnehmer:
        return Teilnehmer(
            nachname=dictionary['Nachname'],
            vorname=dictionary['Vorname'],
            preis=Decimal(dictionary['Zu Bezahlen'] if len(dictionary['Zu Bezahlen']) > 0 else 0),
            bereits_bezahlt=Decimal(dictionary['Bereits Bezahlt']  if len(dictionary['Bereits Bezahlt']) > 0 else 0),
            email_eltern=dictionary['E-Mail Eltern'],
            email_schueler=dictionary['E-Mail Schüler']
        )

    def __lt__(self, other: Teilnehmer):
        if self.nachname == other.nachname:
            return self.vorname < other.vorname
        else:
            return self.nachname < other.nachname

    def associate_ueberweisung(self, ueberweisung: Ueberweisung) -> Teilnehmer:
        return Teilnehmer(
            nachname=self.nachname,
            vorname=self.vorname,
            preis=self.preis,
            bereits_bezahlt=ueberweisung.betrag,
            email_eltern=self.email_eltern,
            email_schueler=self.email_schueler
        )
