# ueberweisungen
# Copyright (C) 2022  Mathezirkel Augsburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import argparse
import pathlib

from util import *


def process_files(camt_file: pathlib.Path, teilnehmer_file: pathlib.Path, output_file: pathlib.Path) -> None:
    with open(camt_file, newline='', encoding='iso-8859-1') as file:
        ueberweisungen: List[Ueberweisung] = parse_ueberweisungen(file)

    with open(teilnehmer_file, newline='', encoding='utf-8-sig') as file:
        teilnehmende: List[Teilnehmer] = parse_teilnehmende(file)

    to_remove, remaining = match_results(ueberweisungen, teilnehmende, 87)
    # Actually remove final bank transfers
    ueberweisungen = list(set(ueberweisungen).difference(to_remove))
    print(f"""
    There are {len(ueberweisungen)} bank transfers not automatically assigned.""")

    manually_check_remaining(remaining, teilnehmende, ueberweisungen)

    teilnehmende.sort()
    with open(output_file, 'w', newline='', encoding='utf-8') as file:
        write_teilnehmende(teilnehmende, file)

    print("""
    The following bank transfers remain:""")
    for ueberweisung in ueberweisungen:
        print(f"{ueberweisung.betrag}€ from {ueberweisung.person} for {ueberweisung.betreff}")


def main() -> None:
    parser = argparse.ArgumentParser(description='Match a CSV-CAMT file of bank transactions to a list of required '
                                                 'payments for the Mathecamp Augsburg.')

    parser.add_argument('--path-camt', type=pathlib.Path)
    parser.add_argument('--path-teilnehmer', type=pathlib.Path)
    parser.add_argument('--path-output', type=pathlib.Path)

    # parser.parse_args()
    args = parser.parse_args(['--path-camt', './data/20220808-251074696-umsatz.CSV',
                              '--path-teilnehmer', './data/bereits-bezahlt.csv',
                              '--path-output', './data/output.csv'])

    process_files(args.path_camt, args.path_teilnehmer, args.path_output)


if __name__ == "__main__":
    main()

