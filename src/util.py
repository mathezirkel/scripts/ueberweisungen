# ueberweisungen
# Copyright (C) 2022  Mathezirkel Augsburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import csv
from typing import TextIO, List, Tuple, Dict, Set, Optional

from thefuzz import process

from model import Ueberweisung, Teilnehmer


def parse_ueberweisungen(file: TextIO) -> List[Ueberweisung]:
    ueberweisungen_reader = csv.DictReader(file, delimiter=';', quotechar='"')
    result: List[Ueberweisung] = []

    for row in ueberweisungen_reader:
        if row['Buchungstext'] == 'GUTSCHR. UEBERWEISUNG':
            result.append(Ueberweisung.from_dict(row))

    return result


def parse_teilnehmende(file: TextIO) -> List[Teilnehmer]:
    teilnehmende_reader = csv.DictReader(file, delimiter=';')
    result: List[Teilnehmer] = []

    for row in teilnehmende_reader:
        result.append(Teilnehmer.from_dict(row))

    return result


def clean(string: str) -> str:
    return string.replace("mathecamp", "").replace("2", "").replace("0", "").strip()


def match_results(ueberweisungen: List[Ueberweisung],
                  teilnehmende: List[Teilnehmer],
                  cutoff: int = 70) -> Tuple[List[Ueberweisung], Dict[Ueberweisung, Set[Teilnehmer]]]:
    unknown: Dict[Ueberweisung, Set[Teilnehmer]] = {}
    remaining_teilnehmende: Dict[Teilnehmer, Tuple[str, str]] = {
        teilnehmer: ((teilnehmer.vorname + " " + teilnehmer.nachname).lower(),
                     (teilnehmer.nachname + " " + teilnehmer.vorname).lower()) for
        teilnehmer in teilnehmende
    }

    to_remove: List[Ueberweisung] = []
    for index, ueberweisung in enumerate(ueberweisungen):
        ueberweisung_matches_1 = process.extractBests(
            query=clean(ueberweisung.betreff.lower()),
            choices=list(zip(*remaining_teilnehmende.values()))[0],
            score_cutoff=cutoff
        )
        ueberweisung_matches_2 = process.extractBests(
            query=clean(ueberweisung.betreff.lower()),
            choices=list(zip(*remaining_teilnehmende.values()))[1],
            score_cutoff=cutoff
        )
        matches: List[Teilnehmer] = list({teilnehmer for teilnehmer, names in remaining_teilnehmende.items() for
                                          name_string, _ in ueberweisung_matches_1 + ueberweisung_matches_2 if
                                          name_string == names[0] or name_string == names[1]})

        if len(matches) == 1:
            # Identify and update Teilnehmer in teilnehmende
            original_teilnehmer = matches[0]
            updated_teilnehmer = original_teilnehmer.associate_ueberweisung(ueberweisung)
            teilnehmende.remove(original_teilnehmer)
            teilnehmende.append(updated_teilnehmer)

            # Make sure that we skip this Teilnehmer at the next search
            remaining_teilnehmende.pop(original_teilnehmer)

            # Remove Ueberweisung as we don't need to consider it again
            to_remove.append(ueberweisung)

            # Log match
            print(f"Matched bank transfer '{ueberweisung.betreff}' to '{original_teilnehmer.vorname} {original_teilnehmer.nachname}'")
        elif len(matches) > 1:
            print(f"Bank transfer '{ueberweisung.betreff}' has more than one option")
            unknown[ueberweisung] = set(matches)

    return to_remove, unknown


def manually_check_remaining(
        remaining: Dict[Ueberweisung, Set[Teilnehmer]],
        teilnehmende: List[Teilnehmer],
        ueberweisungen: List[Ueberweisung]) -> None:
    print("Now please go through the remaining bank transfers!"
          " If none of the answers makes sense, just leave the field empty")
    print(f"""There are {len(remaining)} people to check manually.
""")
    for ueberweisung, candidates in remaining.items():
        user_answer = ask_user(ueberweisung, candidates)
        if user_answer:
            teilnehmende.remove(user_answer)
            teilnehmende.append(user_answer.associate_ueberweisung(ueberweisung))
            ueberweisungen.remove(ueberweisung)


def ask_user(ueberweisung: Ueberweisung, teilnehmende: Set[Teilnehmer]) -> Optional[Teilnehmer]:
    print(f"{ueberweisung.betrag}€ from {ueberweisung.person} for {ueberweisung.betreff}:")
    sorted_teilnehmende = list(teilnehmende)
    for index, teilnehmer in enumerate(sorted_teilnehmende):
        print(f"[{index}] - {teilnehmer.vorname} {teilnehmer.nachname}")
    raw_answer = input()
    if raw_answer == "":
        return None
    try:
        answer = int(raw_answer)
        return sorted_teilnehmende[answer]
    except:
        print("Could not parse answer, ignoring entry")
        return None


def write_teilnehmende(teilnehmende: List[Teilnehmer], output_file: TextIO) -> None:
    fieldnames = ['Nachname', 'Vorname', 'Zu Bezahlen', 'Bereits Bezahlt', 'Differenz', 'E-Mail Eltern',
                  'E-Mail Schüler']
    teilnehmende_writer = csv.DictWriter(output_file, fieldnames=fieldnames)

    teilnehmende_writer.writeheader()
    for teilnehmer in teilnehmende:
        teilnehmende_writer.writerow({'Nachname': teilnehmer.nachname,
                                      'Vorname': teilnehmer.vorname,
                                      'Zu Bezahlen': teilnehmer.preis,
                                      'Bereits Bezahlt': teilnehmer.bereits_bezahlt,
                                      'Differenz': teilnehmer.bereits_bezahlt - teilnehmer.preis,
                                      'E-Mail Eltern': teilnehmer.email_eltern,
                                      'E-Mail Schüler': teilnehmer.email_schueler})
